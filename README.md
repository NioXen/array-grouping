# Array Grouping
## chunkArray
Given an array of length >= 0, and a positive integer N, return the contents of the array divided into N equally sized arrays.  
Where the size of the original array cannot be divided equally by N, the final part should have a length equal to the remainder.

# Installation
There are no run-time dependencies. Development dependencies can be installed using `npm install`

# Unit Tests
Unit tests are written using Jest and can be executed by running `npm run test`

# Compiling to JavaScript
JavaScript will be generated in the `./build` directory by running `npm run compile`

# Usage

### Tarball
If you are installing this from the included `.tgz` file then there is no need to install or compile anything. In a new project, just run `npm install ../path/to/array-grouping-1.0.0.tgz`.

### Building

1. Download the project
2. Run `npm install` to install the development dependencies
3. Run `npm run compile` to build the JavaScript

### Installing as an npm module
In an initialised npm project run `npm install ../array-grouping` where `../array-grouping` is the relative path to your local copy of this project (after compilation)

```javascript
const {chunkArray} = require("array-grouping");
console.log(chunkArray([1,2,3,4,5], 3));
```

### Including directly
In a new JavaScript file, simply require the root of this project as a relative path

```javascript
const {chunkArray} = require("../array-grouping");
console.log(chunkArray([1,2,3,4,5], 3));
```

### In TypeScript

```typescript
import {chunkArray} from "array-grouping";
console.log(chunkArray([1,2,3,4,5], 3));
```