export function chunkArray<T>(sourceArray: T[], chunks: number): T[][] {
  validateInputs(sourceArray, chunks);

  const targetArray = [];
  const chunkLength = calculateChunkLength(sourceArray.length, chunks);
  const nonRemainderChunks = chunks - 1;

  for (let i = 0; i < nonRemainderChunks; i++) {
    const lowerBound = i * chunkLength;
    targetArray.push(sourceArray.slice(lowerBound, lowerBound + chunkLength));
  }
  targetArray.push(sourceArray.slice(nonRemainderChunks * chunkLength));

  return targetArray;
}

function calculateChunkLength(arrayLength: number, chunks: number) {
  if (chunks === 2) {
    return Math.floor(arrayLength / chunks);
  }

  const nonRemainderChunks = isExactlyDivisible(arrayLength, chunks)
    ? chunks
    : chunks - 1;
  const chunkLength = Math.floor(arrayLength / nonRemainderChunks);
  if (
    isExactlyDivisible(arrayLength, nonRemainderChunks) &&
    nonRemainderChunks !== chunks
  ) {
    return chunkLength - 1;
  } else {
    return chunkLength;
  }
}

function validateInputs(array: unknown[], chunks: number) {
  if (!Array.isArray(array)) {
    throw new TypeError('sourceArray must be an array');
  }
  if (!Number.isInteger(chunks)) {
    throw new TypeError('chunks must be an integer');
  }
  if (chunks <= 0) {
    throw new RangeError('chunks must be a positive, non-zero integer');
  }
}

function isExactlyDivisible(numerator: number, denominator: number): boolean {
  return numerator % denominator === 0;
}
