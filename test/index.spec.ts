import {chunkArray} from '../src/index';

type TestCase = {
  sourceArray: number[];
  chunks: number;
  expected: number[][];
};

describe('a utility to slice a given array with length >= 0 into N equally sized arrays', () => {
  describe('input validation', () => {
    describe('chunks must be a positive, non-zero integer', () => {
      const invalidParameterTestCases = [{}, 'aString', true].map(value => [
        typeof value,
        value,
      ]);

      it.each(invalidParameterTestCases)(
        'should throw a TypeError when chunks is a %s',
        (_, input) => {
          expect(() => {
            chunkArray([], input as unknown as number);
          }).toThrow(TypeError);
        }
      );

      it('should throw a TypeError when chunks is a float', () => {
        expect(() => {
          chunkArray([], 5.00001);
        }).toThrow(TypeError);
      });

      it('should throw a RangeError when chunks is negative', () => {
        expect(() => {
          chunkArray([], -1);
        }).toThrow(RangeError);
      });

      it('should throw a RangeError when chunks is 0', () => {
        expect(() => {
          chunkArray([], -1);
        }).toThrow(RangeError);
      });
    });

    describe('source array must be an array', () => {
      const invalidParamterTestCases = [{}, 'aString', 1, true].map(value => [
        typeof value,
        value,
      ]);

      it.each(invalidParamterTestCases)(
        'should throw a TypeError when source array is a %s',
        (_, input) => {
          expect(() => {
            chunkArray(input as unknown[], 1);
          }).toThrow(TypeError);
        }
      );
    });
  });

  describe('chunks is a factor of array length', () => {
    describe.each`
      sourceArray                                            | chunks | expected
      ${[1, 2, 3, 4]}                                        | ${2}   | ${[[1, 2], [3, 4]]}
      ${[1, 2, 3, 4, 5, 6]}                                  | ${3}   | ${[[1, 2], [3, 4], [5, 6]]}
      ${[1, 2, 3, 4, 5]}                                     | ${1}   | ${[[1, 2, 3, 4, 5]]}
      ${[1, 2, 3, 4]}                                        | ${4}   | ${[[1], [2], [3], [4]]}
      ${[1, 2, 3, 4, 5, 6, 7, 8]}                            | ${4}   | ${[[1, 2], [3, 4], [5, 6], [7, 8]]}
      ${[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]} | ${3}   | ${[[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15]]}
    `(
      'given an array of length $sourceArray.length and a number of chunks $chunks',
      ({sourceArray, chunks, expected}: TestCase) => {
        const actual = chunkArray(sourceArray, chunks);

        it('should return an array', () => {
          expect(Array.isArray(actual)).toBe(true);
        });

        it(`should return ${chunks} sub-arrays`, () => {
          expect(actual).toHaveLength(chunks);
        });

        it(`should return ${chunks} sub-arrays of equal length`, () => {
          actual.forEach(chunk => {
            expect(chunk.length).toBe(actual[0].length);
          });
        });

        it(`should return ${JSON.stringify(expected)}`, () => {
          expect(actual).toStrictEqual(expected);
        });
      }
    );
  });

  describe('chunks is not a factor of array length', () => {
    describe.each`
      sourceArray                                            | chunks | expected
      ${[1, 2, 3, 4, 5]}                                     | ${3}   | ${[[1, 2], [3, 4], [5]]}
      ${[1, 2, 3, 4, 5, 6, 7]}                               | ${4}   | ${[[1, 2], [3, 4], [5, 6], [7]]}
      ${[1, 2, 3, 4, 5, 6, 7]}                               | ${5}   | ${[[1], [2], [3], [4], [5, 6, 7]]}
      ${[1, 2, 3, 4, 5]}                                     | ${2}   | ${[[1, 2], [3, 4, 5]]}
      ${[1, 2, 3, 4, 5]}                                     | ${4}   | ${[[1], [2], [3], [4, 5]]}
      ${[1, 2, 3, 4, 5]}                                     | ${6}   | ${[[], [], [], [], [], [1, 2, 3, 4, 5]]}
      ${[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]} | ${4}   | ${[[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15]]}
    `(
      'given an array of length $sourceArray.length and a number of chunks $chunks',
      ({sourceArray, chunks, expected}: TestCase) => {
        const actual = chunkArray(sourceArray, chunks);

        it('should return an array', () => {
          expect(Array.isArray(actual)).toBe(true);
        });

        it(`should return ${chunks} sub-arrays`, () => {
          expect(actual).toHaveLength(chunks);
        });

        it(`should return ${
          chunks - 1
        } sub-arrays of equal length and the last should differ`, () => {
          actual.slice(0, actual.length - 1).forEach(chunk => {
            expect(chunk.length).toBe(actual[0].length);
            expect(chunk.length).not.toBe(actual[actual.length - 1].length);
          });
        });

        it(`should return ${JSON.stringify(expected)}`, () => {
          expect(actual).toStrictEqual(expected);
        });
      }
    );
  });
});
